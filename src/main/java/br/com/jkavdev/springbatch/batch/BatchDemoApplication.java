package br.com.jkavdev.springbatch.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;
import java.io.File;

@EnableBatchProcessing
@SpringBootApplication
public class BatchDemoApplication {

    public static class NaoCliente {
        private String nome, email, cadastro;

        public NaoCliente() {
        }

        public NaoCliente(String nome, String email, String cadastro) {
            this.nome = nome;
            this.email = email;
            this.cadastro = cadastro;
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCadastro() {
            return cadastro;
        }

        public void setCadastro(String cadastro) {
            this.cadastro = cadastro;
        }
    }

    @Bean
    FlatFileItemReader<NaoCliente> fileReader(@Value("${input}") Resource in) {
        return new FlatFileItemReaderBuilder<NaoCliente>()
                .name("file-reader")
                .resource(in)
                .targetType(NaoCliente.class)
                //descobrir porque nao funcionou
//                .delimited().delimiter(",").names(new String[]{"nome, cadastro, email"})
                //tive que fazer deste jeito
                .lineMapper(new DefaultLineMapper<NaoCliente>() {
                    {
                        setLineTokenizer(new DelimitedLineTokenizer() {
                            {
                                setNames("nome",
                                        "cadastro",
                                        "email");
                            }
                        });
                        setFieldSetMapper(new BeanWrapperFieldSetMapper<NaoCliente>() {
                            {
                                setTargetType(NaoCliente.class);
                            }
                        });
                    }
                })
                .build();
    }

    @Bean
    JdbcBatchItemWriter<NaoCliente> jdbcWriter(DataSource ds) {
        return new JdbcBatchItemWriterBuilder<NaoCliente>()
                .dataSource(ds)
                .sql("insert into nao_cliente(nome, cadastro, email) values(:nome, :cadastro, :email)")
                .beanMapped()
                .build();
    }

    @Bean
    Job job(
            JobBuilderFactory jbf,
            StepBuilderFactory sbf,
            ItemReader<? extends NaoCliente> ir,
            ItemWriter<? super NaoCliente> iw) {

        TaskletStep s1 = sbf.get("file-db")
                .<NaoCliente, NaoCliente>chunk(100)
                .reader(ir)
                .writer(iw)
                .build();

        return jbf.get("etl")
                .incrementer(new RunIdIncrementer())
                .start(s1)
                .build();
    }


    public static void main(String[] args) {

        System.setProperty("input", "file:/" + new File("C:/projects/spring-batch/nao-clientes.csv").getAbsolutePath());
        System.setProperty("output", "file:/" + new File("C:/projects/spring-batch/out.csv").getAbsolutePath());

        SpringApplication.run(BatchDemoApplication.class, args);
    }

}

