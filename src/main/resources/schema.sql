drop table if exists nao_cliente;

create table nao_cliente
(
  id       bigint(10)   not null auto_increment primary key,
  nome     varchar(255) not null,
  cadastro varchar(255) not null,
  email    varchar(255) not null
)
